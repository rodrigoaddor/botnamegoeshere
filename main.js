const express = require('express')
const discord = require('discord.js')
const config = require('./config')

const Express = express()
const Client = new discord.Client()
const Bot = {}

Client.on('ready', () => {
  console.log(`Logged in as ${Client.user.tag}`)
})

Client.on('message', async message => {
  if (message.author.bot) return

  for (let cmd of Bot.commands._passives) {
    if (cmd.trigger.test(message.content)) {
      cmd.callback(message, cmd.trigger)
    }
  }

  if (message.content.indexOf(Bot.config.prefix) !== 0) return

  const args = message.content.slice(Bot.config.prefix.length).trim().split(/\s+/g)
  const command = args.shift().toLowerCase()

  if (typeof Bot.commands[command] !== 'undefined')
    Bot.commands[command].callback(message, args)
  else
    message.channel.send(`Unknown command \`${command}\``)
})

Express.all("/api/status", (req, res) => {
  res.status(200).json({
    user: Client.user.tag,
    guilds: Client.guilds.array().length,
    ping: Client.ping,
    uptime: Client.uptime,
  })
})

Express.all("/api/test", (req, res) => {
  let guildList = Client.guilds.array();
  try {
    guildList.forEach(guild => console.log(guild))
  } catch (err) {
    console.log(err);
  }
})

config.fromFile('./config.hjson').then(conf => {
  Bot.config = conf;
  let commands = require(Bot.config['commandsFile'] || './commands')
  Bot.commands = {}
  Bot.commands._passives = []

  for (let cmd of Object.keys(commands)) {
    if (commands[cmd].type === 'passive')
      Bot.commands._passives.push(commands[cmd])
    else
      Bot.commands[commands[cmd].trigger] = commands[cmd]
  }

  console.log(`Loaded ${Object.keys(Bot.commands).length - 1} active command(s) and ${Bot.commands._passives.length} passive command(s).`)

  Express.listen(Bot.config['port'], () => console.log(`Control panel listening on port ${Bot.config['port']}`))
  Client.login(Bot.config['token']).catch(console.error)
})