/*      Commands
 *   Commands are modules that adds functionality into the bot.
 *   Commands do not necessarily need to be called by a user with a prefix, they may also be passive commands.
 *   While active commands are only executed when a message starts with a certain prefix (ignoring the bot's global
 *    prefix), passive commands will be executed when any message that matches the command's regex is received.
 */

const isset = variable => typeof variable !== 'undefined'

module.exports = class Command {
  constructor(options) {
    options.type = options.type || 'active'
    if (!isset(options.name) || !isset(options.trigger) || !isset(options.callback))
      throw Error('Missing parameters in a command constructor')

    if (options.type !== 'active' && options.type !== 'passive')
      throw Error(`Invalid command type (${options.type}) in ${options.name}. Type must be 'active' or 'passive'`)

    if (options.type === 'active' && options.trigger instanceof RegExp)
      throw  Error(`An active command's trigger must not be a regex expression`)

    this.name = options.name
    this.type = options.type
    this.trigger = options.type === 'active' ? options.trigger :
        options.trigger instanceof RegExp ? options.trigger : RegExp(options.trigger)
    this.callback = options.callback
  }
}