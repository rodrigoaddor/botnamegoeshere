const Hjson = require('hjson')
const fs = require('fs');

const isPrivate = field => field.indexOf('_') === 0

module.exports = class Config {
  constructor(hjson) {
    this._data = Hjson.parse(hjson)
    return new Proxy(this, {
      get: (obj, key) => {
        if (isPrivate(key)) throw ReferenceError('Configs must not start with an underscore')
        return this._data[key]
      },
      set: (obj, key, value) => {
        if (isPrivate(key)) throw ReferenceError('Configs must not start with an underscore')
        return this._data[key] = value
      },
      has: (obj, key) => {
        if (isPrivate(key)) return false
        return key in this._data
      },
      deleteProperty: (obj, key) => {
        if (isPrivate(key)) return false
        return delete this._data[key]
      },
      ownKeys: () => {
        return Object.keys(this._data)
      },
      getOwnPropertyDescriptor() {
        return {
          enumerable: true,
          configurable: true,
        };
      }
    })
  }

  static fromFile(file, encoding = 'utf8') {
    return new Promise((res, rej) => {
      fs.readFile(file, encoding, (err, data) => {
        if (err) rej(err)
        res(new Config(data))
      })
    })
  }
}
